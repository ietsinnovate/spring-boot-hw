Setup Docker PostgreSQL database with external volume 

Create the Volume 
docker create -v /var/lib/postgresql/data --name PostgresData alpine

Create and Run the PostgreSQL docker container 
eg. docker run -p 5432:5432 --name yourContainerName -e POSTGRES_PASSWORD=yourPassword -d --volumes-from PostgresData postgres
docker run -p 5432:5432 --name PostgreSQL -e POSTGRES_PASSWORD=PostgresPWD! -d --volumes-from PostgresData postgres

pgAdmin
https://www.pgadmin.org/download/
password - PgadminPWD!


Spring Boot Maven Plugin
https://docs.spring.io/spring-boot/docs/current/maven-plugin/run-mojo.html#profiles

These are different ways to run the spring-boot application so that it uses the 'postgres' profile and uses the postgres database.
There is also a way to activate it through the maven build http://dolszewski.com/spring/spring-boot-properties-per-maven-profile/, which
requires a rebuild each time, so the preference is to use the environment variable approach.  Here is a reference for further investigation
to align the Maven profile and the spring-boot profile https://stackoverflow.com/questions/42390860/configure-active-profile-in-springboot-via-maven

Works in any Terminal
java -jar .\target\rl_rest-api-0.0.1-SNAPSHOT.jar --spring.profiles.active=postgres

Required for Visual Studio Code Terminal 
cmd /c mvn spring-boot:run -D"spring.profiles.active=postgres" -DskipTests -f "c:\Users\vmadmin\record-linking.projects\spring-boot-hw\rl_rest-api\pom.xml"

When the Maven profile is linked to the Spring Boot profile 
cmd /c mvn spring-boot:run -Ppostgres -DskipTests -f "c:\Users\vmadmin\record-linking.projects\spring-boot-hw\rl_rest-api\pom.xml"


Maven Commands 
cmd /c mvn clean install -DskipTests -f "c:\Users\vmadmin\record-linking.projects\spring-boot-hw\rl_rest-api\pom.xml"

cmd /c mvn test -f "c:\Users\vmadmin\record-linking.projects\spring-boot-hw\rl_rest-api\pom.xml" > spring-boot-hw.test-run

Git Commands
git diff origin/master..HEAD 

Docker Commands
Get Images:     docker images
Get Containers: docker ps
Build:          docker build -t < name for image > < directory containing Dockerfile>        // docker build -t rl_rest-api rl_rest-api
Run:            docker run -p < port on local machine>:< port on container> -i -t < image id>       // docker run -p 8080:8080 rl_rest-api
Remove Image:   docker rmi < image id>
Remove Container: docker rm < container id>


