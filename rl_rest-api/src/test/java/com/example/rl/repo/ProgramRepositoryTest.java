package com.example.rl.repo;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

import java.util.List;

import com.example.rl.Util;
import com.example.rl.domain.Program;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@DataJpaTest
public class ProgramRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;
   
    @Autowired
    private ProgramRepository programRepository;

    @Test
    public void testInjectedComponentsAreNotNull() {
        assertThat(entityManager, is(notNullValue()));
        assertThat(programRepository, is(notNullValue()));
    }

    @Test
    public void testProgramRepositoryFindAll() throws Exception {
        Iterable<Program> programIterable = this.programRepository.findAll();
        List<Program> programList = Util.toList(programIterable);
        assertThat(programList.size(), is(not(0)));
    }

    @Test
    public void testProgramCreate() throws Exception {
        Program program = new Program(9000L, "Test Program (TP)");
        programRepository.save(program);
        assertNotNull(programRepository.findById(9000L));
    }

    @Test
    public void testFindByProgramNameEndsWith() throws Exception {
        List<Program> programs = programRepository.findByProgramNameEndsWith("(T1)");
        assertNotNull(programs);
    }
}
