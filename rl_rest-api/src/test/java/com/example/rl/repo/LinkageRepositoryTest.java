package com.example.rl.repo;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@DataJpaTest
public class LinkageRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;
   
    @Autowired
    private LinkageRepository linkageRepository;

    @Test
    public void testInjectedComponentsAreNotNull() {
        assertThat(entityManager, is(notNullValue()));
        assertThat(linkageRepository, is(notNullValue()));
    }

}