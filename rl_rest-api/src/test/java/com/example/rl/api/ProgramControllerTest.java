package com.example.rl.api;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;

import com.example.rl.domain.Program;
import com.example.rl.service.ProgramService;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class ProgramControllerTest {

    private static final String PROGRAM_URL = "programs/";

    private static final Long PROGRAM_ID = new Long(99);
    private static final String PROGRAM_NAME = new String("Program Name (PN)");

    @LocalServerPort
    private int port;

    @MockBean
    private ProgramService programServiceMock;

//    @Mock
    private Program program;
    
    @Autowired
    private TestRestTemplate restTemplate;

    @Before
    public void setupReturnValuesOfMockMethods() {
        MockitoAnnotations.initMocks(this);
        program = new Program(PROGRAM_ID, PROGRAM_NAME);
/*
        // setup program with mock data
        when(program.getId()).thenReturn(PROGRAM_ID);
        when(program.getName()).thenReturn(PROGRAM_NAME);
*/
    }

    @Test
    //@Ignore
    public void testGetPrograms() {
        when(programServiceMock.findAllPrograms())
            .thenReturn(Arrays.asList(program, program));

        ResponseEntity<List<Program>> response = restTemplate
            .exchange("http://localhost:"+port+"/"+PROGRAM_URL, HttpMethod.GET, null, new ParameterizedTypeReference<List<Program>>() {});
        
        assertThat(response.getStatusCode(), is(HttpStatus.OK));
        assertThat(response.getBody().size(), is(2));
    }

    @Test
    //@Ignore
    public void testGetProgramById() {
        when(programServiceMock.findById(1L))
            .thenReturn(program);

        ResponseEntity<Program> response = restTemplate
        .exchange("http://localhost:"+port+"/"+PROGRAM_URL+"1", HttpMethod.GET, null, Program.class);

        assertThat(response.getStatusCode(), is(HttpStatus.OK));
        assertThat(response.getBody().getName(), is(PROGRAM_NAME));
    }

    @After
    public void tearDown() {
        try {
            clearDatabase();
        } catch (Exception e) {
            fail(e.getMessage());
        }
    }
    
    
    public void clearDatabase() throws Exception {
/*
      DataSource ds = (DataSource) SpringApplicationContext.getBean("mydataSource");
      Connection connection = null;
      try {
        connection = ds.getConnection();
        try {
          Statement stmt = connection.createStatement();
          try {
            stmt.execute("TRUNCATE SCHEMA PUBLIC RESTART IDENTITY AND COMMIT NO CHECK");
            connection.commit();
          } finally {
            stmt.close();
          }
        } catch (SQLException e) {
            connection.rollback();
            throw new Exception(e);
        }
        } catch (SQLException e) {
            throw new Exception(e);
        } finally {
            if (connection != null) {
                connection.close();
            }
        }
*/
    }    
}