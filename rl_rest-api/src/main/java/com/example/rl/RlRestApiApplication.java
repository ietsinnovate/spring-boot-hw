/**
 * This package contains the main RecordLinking Application.  
 */
package com.example.rl;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import static springfox.documentation.builders.PathSelectors.any;

@SpringBootApplication
@EnableSwagger2
public class RlRestApiApplication {

	@Bean
    public Docket docket() {
        return new Docket(DocumentationType.SWAGGER_2).select()
                .apis(RequestHandlerSelectors.basePackage("com.example.rl")).paths(any()).build()
				.apiInfo(new ApiInfoBuilder()
					.title("Record Linking Registry")
					.description("A repository of already linked records.")
					.termsOfServiceUrl("CRA Creative Commons")
					.contact(new Contact( "IETS", "https//canada.ca/", "info@cra-arg.gc.ca" )) 
					.license("Private")
					.licenseUrl("https://canada.ca/")
					.version("1.0")
					.build()
					);
    }

	public static void main(String[] args) {
		SpringApplication.run(RlRestApiApplication.class, args);
	}

}
