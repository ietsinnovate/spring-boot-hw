package com.example.rl.service;

import java.util.ArrayList;
import java.util.List;

public class LinkageReadOnly {

    private Long id;
    private List<Long> metaId = new ArrayList<Long>();
    private String guid, timestamp, key1, key2;

    public LinkageReadOnly(Long id, List<Long> metaId, String guid, String timestamp, String key1, String key2) {
        this.id = id;
        this.metaId = metaId;
        this.guid = guid;
        this.timestamp = timestamp;
        this.key1 = key1;
        this.key2 = key2;
    }

    public Long getId() {
        return id;
    }

    public List<Long> getMetaId() {
        return metaId;
    }

    public String getGuid() {
        return guid;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public String getKey1() {
        return key1;
    }

    public String getKey2() {
        return key2;
    }

}