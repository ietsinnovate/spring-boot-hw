package com.example.rl.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.transaction.Transactional;

import com.example.rl.domain.Guids;
import com.example.rl.domain.Key;
import com.example.rl.domain.Linkage;
import com.example.rl.domain.LinkageData;
import com.example.rl.domain.MetaData;
import com.example.rl.repo.GuidsRepository;
import com.example.rl.repo.KeyTypeRepository;
import com.example.rl.repo.KeysRepository;
import com.example.rl.repo.LinkageRepository;
import com.example.rl.repo.LinkageTypeRepository;
import com.example.rl.repo.MetaDataRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class LinkageService {
    
    private LinkageRepository linkageRepository;
    private KeysRepository keysRepository;
    private GuidsRepository guidsRepository;
    private MetaDataRepository metaDataRepository;
    private LinkageTypeRepository linkageTypeRepository;
    private KeyTypeRepository keyTypeRepository;

    @Autowired
    public LinkageService(LinkageRepository linkageRepository,
                            KeysRepository keysRepository, 
                            GuidsRepository guidsRepository, 
                            MetaDataRepository metaDataRepository, 
                            LinkageTypeRepository linkageTypeRepository,
                            KeyTypeRepository keyTypeRepository){
        this.linkageRepository = linkageRepository;
        this.keysRepository = keysRepository;
        this.guidsRepository = guidsRepository;
        this.metaDataRepository = metaDataRepository;
        this.linkageTypeRepository = linkageTypeRepository;
        this.keyTypeRepository = keyTypeRepository;
    }

    public void save(LinkageData link) throws Exception {
        //Linkage linkage =  new Linkage(HashGenerator.getSHA(link.getMetaData()), link.getKey1(), link.getKey2());
        Linkage linkage = new Linkage();
        if(keysRepository.findByKey(link.getKey1()).isEmpty() && keysRepository.findByKey(link.getKey2()).isEmpty()){
            //TODO: add both keys + generate new guid and assign to all linked keys
            Key key1 = new Key();
            Key key2 = new Key();
            Guids guid = new Guids();
            String hashOfKeys = HashGenerator.getSHA(link.getKey1()+link.getKey2());
            
            //save guid
            guid.setGuid(hashOfKeys);
            //TODO: add timestamp attribute
            guid = guidsRepository.save(guid);

            //save keys
            key1.setKey(link.getKey1());
            key1.setGuid_id(guid.getId());
            key1.setProgram_id(Long.parseLong(link.getProgram1()));
            key1.setType_id(keyTypeRepository.findByType(link.getKey1_type()).get(0).getId());
            
            key2.setKey(link.getKey2());
            key2.setGuid_id(guid.getId());
            key2.setProgram_id(Long.parseLong(link.getProgram2()));
            key2.setType_id(keyTypeRepository.findByType(link.getKey2_type()).get(0).getId());

            key1 = keysRepository.save(key1);
            key2 = keysRepository.save(key2);


            //save Linkage
            linkage.setKey1(key1.getId());
            linkage.setKey2(key2.getId());
            linkage.setType(linkageTypeRepository.findByType(link.getType()).get(0).getId());
            linkage.setConfidenceLevel(link.getConfidenceLevel());
            linkageRepository.save(linkage);

            //save metadata
            MetaData meta = new MetaData();
            meta.setKeyId(key1.getId());
            meta.setMetaData(link.getMetaData());
            meta.setTimestamp((new Timestamp(new Date().getTime())).toString());
            metaDataRepository.save(meta);
            MetaData meta2 = new MetaData();
            meta2.setKeyId(key2.getId());
            meta2.setMetaData(link.getMetaData());
            meta2.setTimestamp((new Timestamp(new Date().getTime())).toString());
            metaDataRepository.save(meta2);
        }
        else if(keysRepository.findByKey(link.getKey1()).isEmpty()){
            //add key1 + assign guid of key2 to key1
            Key key1 = new Key();
            Key key2 = keysRepository.findByKey(link.getKey2()).get(0);
            Guids guid = guidsRepository.findById(key2.getGuid_id()).get();
            
            key1.setGuid_id(guid.getId());
            key1.setKey(link.getKey1());
            key1.setProgram_id(Long.parseLong(link.getProgram1()));
            key1.setType_id(keyTypeRepository.findByType(link.getKey1_type()).get(0).getId());
            key1 = keysRepository.save(key1);

            linkage.setKey1(key1.getId());
            linkage.setKey2(key2.getId());
            linkage.setType(linkageTypeRepository.findByType(link.getType()).get(0).getId());
            linkage.setConfidenceLevel(link.getConfidenceLevel());
            linkageRepository.save(linkage);

            //save metadata
            MetaData meta = new MetaData();
            meta.setKeyId(key1.getId());
            meta.setMetaData(link.getMetaData());
            meta.setTimestamp((new Timestamp(new Date().getTime())).toString());
            metaDataRepository.save(meta);
            MetaData meta2 = new MetaData();
            meta2.setKeyId(key2.getId());
            meta2.setMetaData(link.getMetaData());
            meta2.setTimestamp((new Timestamp(new Date().getTime())).toString());
            metaDataRepository.save(meta2);
            
        }
        else if(keysRepository.findByKey(link.getKey2()).isEmpty()){
            //add key2 + assign guid of key1 to key2

            Key key2 = new Key();
            Key key1 = keysRepository.findByKey(link.getKey1()).get(0);
            Guids guid = guidsRepository.findById(key1.getGuid_id()).get();
            
            key2.setGuid_id(guid.getId());
            key2.setKey(link.getKey2());
            key2.setProgram_id(Long.parseLong(link.getProgram2()));
            key2.setType_id(keyTypeRepository.findByType(link.getKey2_type()).get(0).getId());
            key2 = keysRepository.save(key2);

            linkage.setKey1(key1.getId());
            linkage.setKey2(key2.getId());
            linkage.setType(linkageTypeRepository.findByType(link.getType()).get(0).getId());
            linkage.setConfidenceLevel(link.getConfidenceLevel());
            linkageRepository.save(linkage);

            //save metadata
            MetaData meta = new MetaData();
            meta.setKeyId(key1.getId());
            meta.setMetaData(link.getMetaData());
            meta.setTimestamp((new Timestamp(new Date().getTime())).toString());
            metaDataRepository.save(meta);
            MetaData meta2 = new MetaData();
            meta2.setKeyId(key2.getId());
            meta2.setMetaData(link.getMetaData());
            meta2.setTimestamp((new Timestamp(new Date().getTime())).toString());
            metaDataRepository.save(meta2);
        }
        else{//handle linking both existing keys with different guids
            Key key1 = keysRepository.findByKey(link.getKey1()).get(0);
            Key key2 = keysRepository.findByKey(link.getKey2()).get(0);
            
            Guids guid = new Guids();
            guid.setGuid(HashGenerator.getSHA(key1.getKey()+key2.getKey()));
            guid = guidsRepository.save(guid);

            linkage.setKey1(key1.getId());
            linkage.setKey2(key2.getId());
            linkage.setType(linkageTypeRepository.findByType(link.getType()).get(0).getId());
            linkage.setConfidenceLevel(link.getConfidenceLevel());
            linkageRepository.save(linkage);

            keysRepository.updateGuid_id(key1.getGuid_id(), key2.getGuid_id(), guid.getId());

            //save metadata
            MetaData meta = new MetaData();
            meta.setKeyId(key1.getId());
            meta.setMetaData(link.getMetaData());
            meta.setTimestamp((new Timestamp(new Date().getTime())).toString());
            metaDataRepository.save(meta);
            MetaData meta2 = new MetaData();
            meta2.setKeyId(key2.getId());
            meta2.setMetaData(link.getMetaData());
            meta2.setTimestamp((new Timestamp(new Date().getTime())).toString());
            metaDataRepository.save(meta2);

        }    
        
        System.out.println("------------------- *** Added Data to Database*** -------------------");
    }
/*
	public void saveMetaData(Long pk, MetaData metaData) throws Exception {
        Linkage linkage = linkageRepository.findById(pk).get();
        if(!linkage.equals(null)){
            //metaData.setLinkage(linkage);
            metaData.setGuid(HashGenerator.getSHA(metaData.getMetaData()));
            metaData.setTimestamp((new Timestamp(new Date().getTime())).toString());
            //metaDataRepository.save(metaData);
        }
        else throw new Exception("Primary Key does not Exist!");
	}
*/
	public Linkage getLinkage(Long pk) {
        return linkageRepository.findById(pk).get();
	}

	public ArrayList<LinkageData> searchLinkedKeys(String securityKey) {
        //Key key = keysRepository.findByKey(securityKey).get(0);
        //ArrayList<Key> keys = new ArrayList<Key>();
        ArrayList<LinkageData> linkageData = new ArrayList<LinkageData>();
        
        List<Linkage> linkagesList = linkageRepository.findLinkagesByKeyId(keysRepository.findByKey(securityKey).get(0).getId());
        //Iterator<Key> iterator = keysRepository.findAll().iterator();
        Iterator<Linkage> iterator = linkagesList.iterator();
        while(iterator.hasNext()){
            Linkage temp = iterator.next();
            Key key1 = keysRepository.findById(temp.getKey1()).get();
            Key key2 = keysRepository.findById(temp.getKey2()).get();
            LinkageData linkage = new LinkageData();
            linkage.setKey1(key1.getKey());
            linkage.setKey2(key2.getKey());
            linkage.setMetaData(searchLinkedMetaData(securityKey, key2.getProgram_id()).get(0).getMetaData());
            if (key1.getKey().equals(securityKey)){
                linkage.setProgram1("-1");
                linkage.setProgram2(key2.getProgram_id()+"");
            }
            else{
                linkage.setProgram2("-1");
                linkage.setProgram1(key1.getProgram_id()+"");
            }
            linkage.setKey1_type(keyTypeRepository.findById(key1.getType_id()).get().getType());
            linkage.setKey2_type(keyTypeRepository.findById(key2.getType_id()).get().getType());
            linkage.setType(linkageTypeRepository.findById(temp.getType()).get().getType());
            linkage.setConfidenceLevel(temp.getConfidenceLevel());
            linkageData.add(linkage);
                //keys.add(temp);
            
        }
		return linkageData;
    }

    public ArrayList<LinkageData> searchLinkedKeys(String securityKey, long programId){
        
        ArrayList<LinkageData> linkageData = new ArrayList<LinkageData>();
        Key key = keysRepository.findKey(securityKey,programId).get(0);
        //List<Linkage> linkagesList = linkageRepository.findLinkagesByKeyId(key.getId());
        //Iterator<Linkage> linkagesIterator = linkagesList.iterator();
        Iterator<Key> keysIterator = keysRepository.findByGuidId(key.getGuid_id()).iterator();
        //get transitive links
        //linkagesList = linkageRepository.findLinkagesByKeyId(key.getId());
        while(keysIterator.hasNext()){
            Key temp = keysIterator.next();
            if(temp.getId() != key.getId()){
            LinkageData linkage = new LinkageData();
            List<Linkage> a= linkageRepository.findLinkageByBothKeys(key.getId(),temp.getId());/////
            if (a.size()!=0){
                Linkage link = a.get(0);
                Key key1 = keysRepository.findById(link.getKey1()).get();
                Key key2 = keysRepository.findById(link.getKey2()).get();
                linkage.setKey1(key1.getKey());
                linkage.setKey2(key2.getKey());
                linkage.setMetaData(searchLinkedMetaData(key2.getKey(), key2.getProgram_id()).get(0).getMetaData());
                linkage.setProgram1(key1.getProgram_id()+"");
                linkage.setProgram2(key2.getProgram_id()+""); 
                linkage.setKey1_type(keyTypeRepository.findById(key1.getType_id()).get().getType());
                linkage.setKey2_type(keyTypeRepository.findById(key2.getType_id()).get().getType());              
                linkage.setType(linkageTypeRepository.findById(link.getType()).get().getType());
                linkage.setConfidenceLevel(link.getConfidenceLevel());
                linkageData.add(linkage);
            }
            else{
                //Linkage link = a.get(0);
                Key key1 = key;
                Key key2 = temp;
                linkage.setKey1(key1.getKey());
                linkage.setKey2(key2.getKey());
                linkage.setMetaData(searchLinkedMetaData(key2.getKey(), key2.getProgram_id()).get(0).getMetaData());
                linkage.setProgram1(key1.getProgram_id()+"");
                linkage.setProgram2(key2.getProgram_id()+"");  
                linkage.setKey1_type(keyTypeRepository.findById(key1.getType_id()).get().getType());
                linkage.setKey2_type(keyTypeRepository.findById(key2.getType_id()).get().getType());             
                linkage.setType("Transitive");
                linkage.setConfidenceLevel("25% [*]");
                linkageData.add(linkage);
            }
        }
        }
        
        //Iterator<Key> iterator = keysRepository.findAll().iterator();
        /*
        while(linkagesIterator.hasNext()){
            Linkage temp = linkagesIterator.next();
            Key key1 = keysRepository.findById(temp.getKey1()).get();
            Key key2 = keysRepository.findById(temp.getKey2()).get();
            LinkageData linkage = new LinkageData();
            linkage.setKey1(key1.getKey());
            linkage.setKey2(key2.getKey());
            linkage.setMetaData(searchLinkedMetaData(securityKey, key2.getProgram_id()).get(0).getMetaData());
            linkage.setProgram1(key1.getProgram_id()+"");
            linkage.setProgram2(key2.getProgram_id()+"");               
            linkage.setType(temp.getType() + "");
            linkage.setConfidenceLevel(temp.getConfidenceLevel());
            linkageData.add(linkage);
                //keys.add(temp);
            
        }*/
		return linkageData;
    }

    public ArrayList<LinkageData> searchLinkedKeys(String securityKey, long programId, long linkageType){
        
        ArrayList<LinkageData> linkageData = new ArrayList<LinkageData>();
        Key key = keysRepository.findKey(securityKey,programId).get(0);
        //List<Linkage> linkagesList = linkageRepository.findLinkagesByKeyId(key.getId());
        //Iterator<Linkage> linkagesIterator = linkagesList.iterator();
        Iterator<Key> keysIterator = keysRepository.findByGuidId(key.getGuid_id()).iterator();
        //get transitive links
        //linkagesList = linkageRepository.findLinkagesByKeyId(key.getId());
        while(keysIterator.hasNext()){
            Key temp = keysIterator.next();
            if(temp.getId() != key.getId()){
                LinkageData linkage = new LinkageData();
                List<Linkage> a= linkageRepository.findLinkageByBothKeys(key.getId(),temp.getId());/////
                if (a.size()!=0){
                    Linkage link = a.get(0);
                    if(link.getType() == linkageType &&  linkageType!=0){
                        Key key1 = keysRepository.findById(link.getKey1()).get();
                        Key key2 = keysRepository.findById(link.getKey2()).get();
                        linkage.setKey1(key1.getKey());
                        linkage.setKey2(key2.getKey());
                        linkage.setMetaData(searchLinkedMetaData(key2.getKey(), key2.getProgram_id()).get(0).getMetaData());
                        linkage.setProgram1(key1.getProgram_id()+"");
                        linkage.setProgram2(key2.getProgram_id()+""); 
                        linkage.setKey1_type(keyTypeRepository.findById(key1.getType_id()).get().getType());
                        linkage.setKey2_type(keyTypeRepository.findById(key2.getType_id()).get().getType());              
                        linkage.setType(linkageTypeRepository.findById(link.getType()).get().getType());
                        linkage.setConfidenceLevel(link.getConfidenceLevel());
                        linkageData.add(linkage);
                    }
                }
                else if (linkageType == 0){
                    Key key1 = key;
                    Key key2 = temp;
                    linkage.setKey1(key1.getKey());
                    linkage.setKey2(key2.getKey());
                    linkage.setMetaData(searchLinkedMetaData(key2.getKey(), key2.getProgram_id()).get(0).getMetaData());
                    linkage.setProgram1(key1.getProgram_id()+"");
                    linkage.setProgram2(key2.getProgram_id()+"");  
                    linkage.setKey1_type(keyTypeRepository.findById(key1.getType_id()).get().getType());
                    linkage.setKey2_type(keyTypeRepository.findById(key2.getType_id()).get().getType());             
                    linkage.setType("Transitive");
                    linkage.setConfidenceLevel("25% [*]");
                    linkageData.add(linkage);

                }
            }
        }
        return linkageData;
    }

    public ArrayList<LinkageData> searchAuthorizedLinks(String securityKey, long programId, long linkageType) {
        ArrayList<LinkageData> linkageData = new ArrayList<LinkageData>();
        Key key = keysRepository.findKey(securityKey,programId).get(0);
        //List<Linkage> linkagesList = linkageRepository.findLinkagesByKeyId(key.getId());
        //Iterator<Linkage> linkagesIterator = linkagesList.iterator();
        Iterator<Key> keysIterator = keysRepository.findByGuidId(key.getGuid_id()).iterator();
        //get transitive links
        //linkagesList = linkageRepository.findLinkagesByKeyId(key.getId());
        while(keysIterator.hasNext()){
            Key temp = keysIterator.next();
            if(temp.getId() != key.getId()){
                LinkageData linkage = new LinkageData();
                List<Linkage> a= linkageRepository.findLinkageByBothKeys(key.getId(),temp.getId());/////
                if (a.size()!=0){
                    Linkage link = a.get(0);
                    if(link.getType() == linkageType &&  linkageType!=0){
                        Key key1 = keysRepository.findById(link.getKey1()).get();
                        Key key2 = keysRepository.findById(link.getKey2()).get();
                        linkage.setKey1(key1.getKey());
                        linkage.setKey2(key2.getKey());
                        linkage.setMetaData(searchLinkedMetaData(key2.getKey(), key2.getProgram_id()).get(0).getMetaData());
                        linkage.setProgram1(key1.getProgram_id()+"");
                        linkage.setProgram2(key2.getProgram_id()+""); 
                        linkage.setKey1_type(keyTypeRepository.findById(key1.getType_id()).get().getType());
                        linkage.setKey2_type(keyTypeRepository.findById(key2.getType_id()).get().getType());              
                        linkage.setType(linkageTypeRepository.findById(link.getType()).get().getType());
                        linkage.setConfidenceLevel(link.getConfidenceLevel());
                        linkageData.add(linkage);
                    }
                }
                else{
                    Key key1 = key;
                    Key key2 = temp;
                    linkage.setKey1(key1.getKey());
                    linkage.setKey2(key2.getKey());
                    linkage.setMetaData(searchLinkedMetaData(key2.getKey(), key2.getProgram_id()).get(0).getMetaData());
                    linkage.setProgram1(key1.getProgram_id()+"");
                    linkage.setProgram2(key2.getProgram_id()+"");  
                    linkage.setKey1_type(keyTypeRepository.findById(key1.getType_id()).get().getType());
                    linkage.setKey2_type(keyTypeRepository.findById(key2.getType_id()).get().getType());             
                    linkage.setType("Transitive");
                    linkage.setConfidenceLevel("25% [*]");
                    linkageData.add(linkage);

                }
            }
        }
        return linkageData;
	}

    public List<Linkage> findAllLinkages() {
        List<Linkage> listOfLinkages = new ArrayList<Linkage>();
        linkageRepository.findAll().forEach(listOfLinkages::add);
        return listOfLinkages;
    }

	public List<MetaData> searchLinkedMetaData(String k, long pId) {
        List<Key> keys = keysRepository.findByKey(k);
        Key key = keysRepository.findByKey(k).get(0);
        for(int i = 0; i < keys.size(); i ++){
            if (keys.get(i).getProgram_id()==pId)
                key = keys.get(i);
        }
        //Guids guid = guidsRepository.findById(key.getGuid_id()).get();
        List<MetaData> metas = metaDataRepository.findByKeyId(key.getId());
        return metas;
    }
    
    public Boolean deleteLinkage(String key1, long programId1, String key2, long programId2){
        try{
            Key keyObject1 = keysRepository.findKey(key1, programId1).get(0);
            Key keyObject2 = keysRepository.findKey(key2, programId2).get(0);
            long k1 = keyObject1.getId();
            long k2 = keyObject2.getId();
            keysRepository.updateGuid_id(keyObject1.getGuid_id(), keyObject2.getGuid_id(), -1);
            linkageRepository.deleteLinkage(k1, k2);
        }
        catch(Exception e){
            return false;
        }
        return true;
    }
    /*
    public List<LinkageReadOnly> findLinkedRecordsBy(String securityId) {
        List<Linkage> linkages = linkageRepository.findLinkedRecordsBy(securityId);
        return flattenLinkages(linkages);
    }

    private List<LinkageReadOnly> flattenLinkages(List<Linkage> linkages) {
        List<LinkageReadOnly> result = new ArrayList<LinkageReadOnly>();
        Iterator<Linkage> iterator = linkages.iterator();
        while (iterator.hasNext()) {
            extracted(result, iterator);
        }
        return result;
    }

	private void extracted(List<LinkageReadOnly> result, Iterator<Linkage> iterator) {
		Linkage linkage = iterator.next();

		List<Long> metaDataList = new ArrayList<Long>();
		for(int i = 0; i< linkage.getListOfMetaData().size(); i++){
		    metaDataList.add(linkage.getListOfMetaData().get(i).getId());
		}
   
		LinkageReadOnly link = new LinkageReadOnly(
		    linkage.getId(),
		    metaDataList,
		    linkage.getGuid(),
		    linkage.getTimestamp(),
		    linkage.getKey1(),
		    linkage.getKey2()
		    );
		result.add(link);
	}
*/

	
    

}