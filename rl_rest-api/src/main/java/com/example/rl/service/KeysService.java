package com.example.rl.service;

import javax.transaction.Transactional;

import com.example.rl.domain.Key;
import com.example.rl.repo.KeysRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class KeysService {
    
    private KeysRepository keysRepository;

    @Autowired
    public KeysService(KeysRepository keysRepository) {
        this.keysRepository = keysRepository;
    }

    public Key getKey(Long id){
        try {
            return keysRepository.findById(id).get();
        } catch (Exception e) {
            return null;
        }
        
    }
}