package com.example.rl.service;

import java.util.ArrayList;
import java.util.List;

import com.example.rl.domain.LinkageType;
import com.example.rl.repo.LinkageTypeRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class LinkageTypeService {

    private LinkageTypeRepository linkageTypeRepository;

    @Autowired
    public LinkageTypeService(LinkageTypeRepository linkageTypeRepository) {
        this.linkageTypeRepository = linkageTypeRepository;
    }

    public List<LinkageType> findAllTypes() {
        List<LinkageType> listOfPrograms = new ArrayList<LinkageType>();
        Iterable<LinkageType> iterable = linkageTypeRepository.findAll();
        iterable.forEach(listOfPrograms::add);        
        return listOfPrograms;
    }

	public long addLinkageType(LinkageType type) {
        return linkageTypeRepository.save(type).getId();
	}
}