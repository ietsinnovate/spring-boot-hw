package com.example.rl.service;

import java.util.ArrayList;
import java.util.List;

import com.example.rl.domain.KeyType;
import com.example.rl.repo.KeyTypeRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class KeyTypeService {

    private KeyTypeRepository keyTypeRepository;

    @Autowired
    public KeyTypeService(KeyTypeRepository keyTypeRepository) {
        this.keyTypeRepository = keyTypeRepository;
    }

    public List<KeyType> findAllTypes() {
        List<KeyType> listOfPrograms = new ArrayList<KeyType>();
        Iterable<KeyType> iterable = keyTypeRepository.findAll();
        iterable.forEach(listOfPrograms::add);        
        return listOfPrograms;
    }

	public long addKeyType(KeyType keyType) {
        return keyTypeRepository.save(keyType).getId();
	}

}