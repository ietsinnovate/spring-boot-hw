package com.example.rl.service;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import com.example.rl.domain.Program;
import com.example.rl.repo.ProgramRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class ProgramService {

    private ProgramRepository programRepository;

    @Autowired
    public ProgramService(ProgramRepository programRepository) {
        this.programRepository = programRepository;
    }

    public List<Program> findAllPrograms() {
        List<Program> listOfPrograms = new ArrayList<Program>();
        Iterable<Program> programIterable = programRepository.findAll();
        programIterable.forEach(listOfPrograms::add);        
        return listOfPrograms;
    }

    public Program findById(Long id) {
        return programRepository.findById(id).orElseThrow(() -> 
            new NoSuchElementException("Program id " + id)
        );
    }

    public List<Program> findByName(String name) {
        return programRepository.findByName(name);
    }

    public List<Program> findProgramsEndingWith(String endsWith) {
        return programRepository.findByProgramNameEndsWith(endsWith);
    }

	public long addProgram(Program program) {
        return programRepository.save(program).getId();
	}
}