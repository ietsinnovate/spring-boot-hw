package com.example.rl.service;

import com.example.rl.domain.MetaData;
import com.example.rl.repo.MetaDataRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class MetaDataService{
    private MetaDataRepository metaDataRepository;

    @Autowired
    public MetaDataService (MetaDataRepository metaDataRepository){
        this.metaDataRepository = metaDataRepository;
    }

    public void save(MetaData metaData){
        metaDataRepository.save(metaData);
    }
}