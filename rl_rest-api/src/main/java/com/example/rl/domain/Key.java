package com.example.rl.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "keys")
public class Key {

    @Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name="key", length = 100)
    private String key;

    @Column(name="guid_id")
    private long guid_id;

    @Column(name="program_id")
    private long program_id;

    @Column(name="type_id")
    private long type_id;

    public long getId(){
        return id;
    }

    public void setId(long id){
        this.id = id;
    }

    public String getKey(){
        return key;
    }

    public void setKey(String k){
        this.key = k;
    }

    public long getGuid_id(){
        return guid_id;
    }

    public void setGuid_id(long g){
        this.guid_id = g;
    }

    public long getProgram_id(){
        return program_id;
    }

    public void setProgram_id(long p){
        this.program_id = p;
    }

    public long getType_id(){
        return this.type_id;
    }

    public void setType_id(long t){
        this.type_id = t;
    }

}