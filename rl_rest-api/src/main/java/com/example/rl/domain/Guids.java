package com.example.rl.domain;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "guids")
public class Guids {

    @Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name="guid", length = 100)
    private String guid;

    public Long getId(){
        return id;
    }

    public void setGuid(String g){
        guid = g;
    }

    public String getGuid(){
        return guid;
    }
}