package com.example.rl.domain;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "program")
public class Program {

    @Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name="name", length = 100)
    private String name;

    public Program() {/* only used for JPA */}

    public Program(long id, String name) {
        this.id = id;
        this.name = name;        
    }

    public long getId() {
        return id;
    }

    public void setId(String id) {
        setId(new Long(id));
    }
    
    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
}
