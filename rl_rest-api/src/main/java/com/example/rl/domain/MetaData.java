package com.example.rl.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "meta_data")
public class MetaData {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "key_id", length = 100)
    private long key_id;

    @Column(name = "meta_data", length = 100)
    private String metaData;

    @Column(name = "timestamp", length =  100)
    private String timestamp;
    
    public MetaData() {/* only used for JPA */}

    public Long getId(){
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    
    public long getKeyId(){
        return key_id;
    }

    public void setKeyId(long key_id){
        this.key_id = key_id;
    }

    public String getMetaData(){
        return metaData;
    }

    public void setMetaData(String md){
        this.metaData = md;
    }

    public String getTimestamp(){
        return timestamp;
    }

    public void setTimestamp(String t){
        timestamp = t;
    }

}