package com.example.rl.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "linkage")
public class Linkage {

    @Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name="key1", length = 100)
    private Long key1;

    @Column(name="key2", length = 100)
    private Long key2;

    @Column(name="type")
    private long type;

    @Column(name="confidence_level")
    private String confidenceLevel;

    public Linkage() {/* only used for JPA */}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getKey1() {
        return key1;
    }

    public void setKey1(Long key1) {
        this.key1 = key1;
    }

    public Long getKey2() {
        return key2;
    }

    public void setKey2(Long key2) {
        this.key2 = key2;
    }

    public void setType(long type){
        this.type= type;
    }

    public long getType(){
        return type;
    }

    public void setConfidenceLevel(String level){
        this.confidenceLevel = level;
    }

    public String getConfidenceLevel(){
        return confidenceLevel;
    }
   
}
