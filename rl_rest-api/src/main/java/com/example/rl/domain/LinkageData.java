package com.example.rl.domain;


public class LinkageData {

    private String key1, key2, metaData, program1, program2, key1_type, key2_type, type, confidenceLevel;

    public String getKey1(){
        return key1;
    }

    public void setKey1(String key1){
        this.key1 = key1;
    }

    public String getKey2(){
        return key2;
    }

    public void setKey2(String key2){
        this.key2 = key2;
    }

    public String getMetaData(){
        return metaData;
    } 

    public void setMetaData(String meta){
        this.metaData = meta;
    } 

    public String getProgram1(){
        return program1;
    }

    public void setProgram1(String prog){
        this.program1 = prog;
    }

    public String getProgram2(){
        return program2;
    }

    public void setProgram2(String prog){
        this.program2 = prog;
    }

    public String getKey1_type(){
        return this.key1_type;
    }

    public void setKey1_type(String k1_type){
        this.key1_type = k1_type;
    }

    public String getKey2_type(){
        return this.key2_type;
    }

    public void setKey2_type(String k2_type){
        this.key2_type = k2_type;
    }

    public String getType(){
        return type;
    }

    public void setType(String type){
        this.type = type;
    }

    public String getConfidenceLevel(){
        return confidenceLevel;
    }

    public void setConfidenceLevel(String cl){
        this.confidenceLevel = cl;
    }
}