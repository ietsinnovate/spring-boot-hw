/**
 * This package contains all domain objects
 */
package com.example.rl.domain;

import java.util.HashMap;

public class TombstoneModel {

	private String firstName, lastName, dob, pobCountry, pobProvince, pobCity, gender, maritialStatus;

	private int programId,securityId;

	public TombstoneModel() {
		firstName="";
		lastName="";
		programId=0;
		dob="";
		pobCountry="";
		pobProvince="";
		pobCity="";
		gender="";
		maritialStatus="";
		securityId=0;
	}
	
	public TombstoneModel(String firstName,String lastName,int programId, String dob,String pobCountry,String pobProvince,String pobCity,String gender,String maritialStatus, int securityId){
		this.firstName=firstName;
		this.lastName=lastName;
		this.programId=programId;
		this.dob=dob;
		this.pobCountry=pobCountry;
		this.pobProvince=pobProvince;
		this.pobCity=pobCity;
		this.gender=gender;
		this.maritialStatus=maritialStatus;
		this.securityId=securityId;
	}
	
	public HashMap<Object, Object> toJSON(){
		HashMap<Object, Object> map = new HashMap<>();
		map.put("firstName", firstName);
		map.put("lastName", lastName);
		map.put("programID", programId);
		map.put("dob_string", dob);
		map.put("pob_country", pobCountry);
		map.put("pob_province", pobProvince);
		map.put("pob_city", pobCity);
		map.put("gender", gender);
		map.put("maritialStatus", maritialStatus);
		map.put("securityID", securityId);
		return map;
	}
}