package com.example.rl.repo;

import java.util.List;

import com.example.rl.domain.Linkage;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface LinkageRepository extends CrudRepository<Linkage, Long>{
    /**
     * Default Repository Methods are needed for now
     */
    @Query(value="select l from Linkage l where l.key1 = :securityId or l.key2 = :securityId")
    List<Linkage> findLinkagesByKeyId(long securityId);

    @Query(value="select l from Linkage l where (l.key1 = :k1 and l.key2 = :k2) or (l.key2 = :k1 and l.key1 = :k2)")
    List<Linkage> findLinkageByBothKeys(long k1, long k2);

    @Modifying
    @Query(value="delete from Linkage l where (l.key1 = :k1 and l.key2 = :k2) or (l.key2 = :k1 and l.key1 = :k2)")
    void deleteLinkage(long k1, long k2);
}