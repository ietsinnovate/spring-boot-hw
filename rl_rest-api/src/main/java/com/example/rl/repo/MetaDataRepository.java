package com.example.rl.repo;

import java.util.List;

import com.example.rl.domain.MetaData;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface MetaDataRepository extends CrudRepository<MetaData, Long>{
    /**
     * Default Repository Methods are needed for now
     */
    @Query(value="select m from MetaData m where m.key_id = :key")
    List<MetaData> findByKeyId(Long key);

}