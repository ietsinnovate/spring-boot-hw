package com.example.rl.repo;

import java.util.List;

import com.example.rl.domain.Guids;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface GuidsRepository extends CrudRepository<Guids, Long>{
    @Query(value="select g from Guids g where g.guid = :guid")
    List<Guids> findByGuid(String guid);
}