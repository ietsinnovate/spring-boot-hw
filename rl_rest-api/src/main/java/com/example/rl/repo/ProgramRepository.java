package com.example.rl.repo;

import java.util.List;

import com.example.rl.domain.Program;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface ProgramRepository extends CrudRepository<Program, Long> {

    /**
     * right now only the default JpaRepository methods are needed
     */

    @Query("select p from Program p where p.name like :endsWith")
    List<Program> findByProgramNameEndsWith(String endsWith);

    @Query("select p from Program p where p.name = :name")
    List<Program> findByName(String name);

}