package com.example.rl.repo;

import java.util.List;

import com.example.rl.domain.LinkageType;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface LinkageTypeRepository extends CrudRepository<LinkageType, Long> {

    @Query(value="select t from LinkageType t where t.type =:type")
    List<LinkageType> findByType(String type);
    
}