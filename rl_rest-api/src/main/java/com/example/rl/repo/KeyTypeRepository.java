package com.example.rl.repo;

import java.util.List;

import com.example.rl.domain.KeyType;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface KeyTypeRepository extends CrudRepository<KeyType, Long> {

    @Query(value="select t from KeyType t where t.type =:type")
    List<KeyType> findByType(String type);
}