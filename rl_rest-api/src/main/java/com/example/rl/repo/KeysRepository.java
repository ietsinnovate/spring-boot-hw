package com.example.rl.repo;

import java.util.List;

import com.example.rl.domain.Key;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface KeysRepository extends CrudRepository<Key, Long>{
    /**
     * Default Repository Methods are needed for now
     */

    @Query(value="select k from Key k where k.key = :key")
    List<Key> findByKey(String key);

    @Modifying
    @Query(value="update Key k set k.guid_id =:newGuid where k.guid_id =:oldGuid1 or k.guid_id =:oldGuid2")//update guid of keys
    void updateGuid_id(long oldGuid1, long oldGuid2, long newGuid);

    @Query(value="select k from Key k where k.key = :key and k.program_id =:prog_id")
    List<Key> findKey(String key, Long prog_id);

    @Query(value="select k from Key k where k.guid_id =:guidId")
    List<Key> findByGuidId(Long guidId);
}