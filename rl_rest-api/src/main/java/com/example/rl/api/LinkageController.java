package com.example.rl.api;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import com.example.rl.domain.Linkage;
import com.example.rl.domain.LinkageData;
import com.example.rl.domain.MetaData;
import com.example.rl.service.LinkageService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@CrossOrigin
@RestController
@RequestMapping(path = "/linkages")
public class LinkageController {
    private LinkageService linkageService;

    @Autowired
    public LinkageController(LinkageService linkageService){
        this.linkageService = linkageService;
    }
    

    @ApiOperation(value = "Add a new Linkage")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "OK"), @ApiResponse(code = 404, message = "Failed to create Linkage") })
    @PostMapping(value="/")
    public void createLinkage(@RequestBody LinkageData link) throws Exception{
        linkageService.save(link);
    }

    @ApiOperation(value = "Add a new MetaData to a Linkage")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "OK"), @ApiResponse(code = 404, message = "Failed to add MetaData to the Linkage") })
    @PostMapping(value="/{pk}/metas")
    public void addMetaDataToLinkage(@RequestBody MetaData metaData, @PathVariable(value="pk") long pk) throws Exception {
        //linkageService.saveMetaData(pk, metaData);
    }

    @ApiOperation(value = "Find a Linkage")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "OK"), @ApiResponse(code = 404, message = "Couldn't find Linkage") })
    @GetMapping(value="/{pk}")
    public Linkage getLinkage(@PathVariable(value = "pk") long pk) {
        return linkageService.getLinkage(pk);
    }
    
    @ApiOperation(value = "Find associated Linkages given a key")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "OK"), @ApiResponse(code = 404, message = "Error finding associated Linkages") })
    @GetMapping(value="/search/{key}")
    public ArrayList<LinkageData> searchLinkages(@PathVariable(value = "key") String key){
        return linkageService.searchLinkedKeys(key);
    }

    @ApiOperation(value = "Find associated Linkages given a key and programId")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "OK"), @ApiResponse(code = 404, message = "Error finding associated Linkages") })
    @GetMapping(value="/search/{key}/{programId}")
    public ArrayList<LinkageData> searchLinkages(@PathVariable(value = "key") String key, @PathVariable(value = "programId") long programId){
        return linkageService.searchLinkedKeys(key,programId);
    }

    @ApiOperation(value = "Find associated Linkages given a key, programId and linkageType")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "OK"), @ApiResponse(code = 404, message = "Error finding associated Linkages") })
    @GetMapping(value="/search/{key}/{programId}/{linkageType}")
    public ArrayList<LinkageData> searchLinkages(@PathVariable(value = "key") String key, @PathVariable(value = "programId") long programId,@PathVariable(value = "linkageType") long linkageType){
        return linkageService.searchLinkedKeys(key,programId, linkageType);
    }

    @ApiOperation(value = "Find associated Linkages given a key, programId and authorization")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "OK"), @ApiResponse(code = 404, message = "Error finding associated Linkages") })
    @GetMapping(value="/search/authorizedLinkages/{auth}/{key}/{programId}")
    public ArrayList<LinkageData> searchAuthorizedLinkages(@PathVariable(value = "key") String key, @PathVariable(value = "programId") long programId,@PathVariable(value = "auth") long linkageType){
        return linkageService.searchAuthorizedLinks(key,programId, linkageType);
    }

    @ApiOperation(value = "Find all Linkages")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "OK"), @ApiResponse(code = 404, message = "Couldn't find Linkage") })
    @GetMapping(path="/")
    public List<Linkage> getAll() {
		List<Linkage> linkages = linkageService.findAllLinkages();
        return linkages;
    }

    @ApiOperation(value = "Find associated MetaData given a key and program_id")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "OK"), @ApiResponse(code = 404, message = "Error finding associated MetaData") })
    @GetMapping(value="/searchMeta/{key}/{programId}")
    public List<MetaData> searchMetaData(@PathVariable(value = "key") String key, @PathVariable(value = "programId") long programId){
        return linkageService.searchLinkedMetaData(key,programId);
    }

    @ApiOperation(value = "Delete a linkage given 2 keys and their program ids")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "OK"), @ApiResponse(code = 404, message = "Error Deleting Linkage") })
    @DeleteMapping(value="/delete/{key1}/{programId1}/{key2}/{programId2}")
    public Boolean deleteLinkage(@PathVariable(value = "key1") String key1, @PathVariable(value = "programId1") long programId1,@PathVariable(value = "key2") String key2, @PathVariable(value = "programId2") long programId2){
        return linkageService.deleteLinkage(key1, programId1, key2, programId2);
    }

    /**
     * Exception handler if NoSuchElementException is thrown in this Controller
     *
     * @param ex exception
     * @return Error message String.
     */
    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(NoSuchElementException.class)
    public String return400(NoSuchElementException ex) {

        return ex.getMessage();
    }
    
}