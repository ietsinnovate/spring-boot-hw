/**
 * This package contains all the controllers that expose a REST API
 */
package com.example.rl.api;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.example.rl.domain.TombstoneModel;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.CrossOrigin;

@CrossOrigin
@RestController
public class RlRestApiController {
	
	
	@GetMapping("/")
    public Map<Object,Object> index() {
		
		TombstoneModel[] models = new TombstoneModel[4];
		models[0] = new TombstoneModel("Rithik", "Sowdermett", 12345, "10/23/1999", "CA","ON","Toronto", "Male", "Single", 987456);
		models[1] = new TombstoneModel("Jason", "Ford", 54321, "05/05/2000", "CA","ON","Peterborough", "Male", "Single", 54561);
		models[2] = new TombstoneModel("Mark", "Bristol", 93480, "09/09/1989", "CA","ON","Ottawa", "Male", "Married", 483321);
		models[3] = new TombstoneModel("Jaime", "Watson", 10632, "01/01/2001", "CA","ON","Niagara", "Male", "Single", 7630125);
		
		ArrayList<HashMap<Object,Object>> list = new ArrayList<>();
		HashMap<Object,Object> temp = new HashMap<>();

		for(int i = 0; i< models.length; i++){
		list.add(models[i].toJSON());
		}
		
		temp.put("tombstone_array",list);
		
		System.out.println(temp);
		
		return temp;
		
    }
    
}