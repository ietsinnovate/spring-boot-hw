package com.example.rl.api;

import java.util.List;
import java.util.NoSuchElementException;

import com.example.rl.domain.KeyType;
import com.example.rl.service.KeyTypeService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;

@CrossOrigin
@Api(description = "API to pull linkage-types")
@RestController
@RequestMapping(path = "/key-types")
public class KeyTypeController {

    private KeyTypeService keyTypeService;

    @Autowired
    public KeyTypeController(KeyTypeService keyTypeService) {
        this.keyTypeService = keyTypeService;
    }

    @ApiOperation(value = "Find all Types")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "OK"), @ApiResponse(code = 404, message = "No Types found") })
    @GetMapping(path="/")
    public List<KeyType> getAll() {
        return keyTypeService.findAllTypes();
    }

    @ApiOperation(value = "Add new keyType")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "OK"), @ApiResponse(code = 404, message = "No Types found") })
    @PostMapping(path="/")
    public long addKeyType(@RequestBody KeyType type) {
        return keyTypeService.addKeyType(type);
    }

    /**
     * Exception handler if NoSuchElementException is thrown in this Controller
     *
     * @param ex exception
     * @return Error message String.
     */
    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(NoSuchElementException.class)
    public String return400(NoSuchElementException ex) {
        return ex.getMessage();
    }
}
