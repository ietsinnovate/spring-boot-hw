package com.example.rl.api;

import java.util.List;
import java.util.NoSuchElementException;

import com.example.rl.domain.Program;
import com.example.rl.service.ProgramService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;

@CrossOrigin
@Api(description = "API to pull programs")
@RestController
@RequestMapping(path = "/programs")
public class ProgramController {

    private ProgramService programService;

    @Autowired
    public ProgramController(ProgramService programService) {
        this.programService = programService;
    }

    @ApiOperation(value = "Find all Programs")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "OK"), @ApiResponse(code = 404, message = "No Programs found") })
    @GetMapping(path="/")
    public List<Program> getAll() {
        return programService.findAllPrograms();
    }

    @ApiOperation(value = "Search across all Programs")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "OK"), @ApiResponse(code = 404, message = "No Programs found") })
    @GetMapping(path="/search")
    public List<Program> searchPrograms(@ApiParam(name = "endsWith", required = false) @RequestParam(value="endsWith", required = false) String endsWith, 
                                        @ApiParam(name = "name", required = false) @RequestParam(value="name") String name) {
        // this can be improved but for now it's illustrative
        if (endsWith != null && !endsWith.isEmpty()) {
            return programService.findProgramsEndingWith(endsWith);
        }
        if (name != null && !name.isEmpty()) {
            return programService.findByName(name);
        }
        return this.getAll();
    }

    @GetMapping(path="/{programId}")
    @ApiOperation(value = "Find a program by id")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "OK"), @ApiResponse(code = 404, message = "Program not found") })
    public Program findById(@PathVariable(value="programId") Long id) {
        return programService.findById(id);
    }    

    @ApiOperation(value = "Add new program")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "OK"), @ApiResponse(code = 404, message = "Program not added") })
    @PostMapping(path="/")
    public long addLinkageType(@RequestBody Program program) {
        return programService.addProgram(program);
    }

    /**
     * Exception handler if NoSuchElementException is thrown in this Controller
     *
     * @param ex exception
     * @return Error message String.
     */
    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(NoSuchElementException.class)
    public String return400(NoSuchElementException ex) {
        return ex.getMessage();
    }
}
