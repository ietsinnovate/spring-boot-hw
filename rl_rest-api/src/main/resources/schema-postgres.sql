CREATE TABLE program(
    id SERIAL NOT NULL, -- primary key column
    name VARCHAR(100) NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE guids(
    id SERIAL NOT NULL, -- primary key column
    guid VARCHAR(100) NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE keys(
    id SERIAL NOT NULL, -- primary key column
    key VARCHAR(100),
    guid_id BIGINT,
    program_id BIGINT,
    PRIMARY KEY (id),
    FOREIGN KEY (guid_id) REFERENCES guids(id),
    FOREIGN KEY (program_id) REFERENCES program(id)
);

CREATE TABLE linkage(
    id SERIAL NOT NULL, -- primary key column
    key1 BIGINT,
    key2 BIGINT,
    PRIMARY KEY (id),
    FOREIGN KEY (key1) REFERENCES keys(id),
    FOREIGN KEY (key2) REFERENCES keys(id)
);

CREATE TABLE meta_data(
    id SERIAL NOT NULL, -- primary key column
    guid_id BIGINT,
    meta_data VARCHAR,
    timestamp VARCHAR,
    PRIMARY KEY (id),
    FOREIGN KEY (guid_id) REFERENCES guids(id)
);