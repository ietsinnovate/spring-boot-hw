INSERT INTO program (name) VALUES 
    ('T1');
INSERT INTO program (name) VALUES 
    ('GST/HST');
INSERT INTO program (name) VALUES 
    ('T3');
INSERT INTO program (name) VALUES 
    ('Non Resident');
INSERT INTO program (name) VALUES 
    ('Childrens Allowance');

INSERT INTO linkage_type (type) VALUES 
    ('CRA');
INSERT INTO linkage_type (type) VALUES 
    ('NON-CRA');

INSERT INTO guids(guid) VALUES ('7afd036ceb16908bf877aec9b5e49fb9b07525910cf7a8d0f1dfd7c9ec8f970b');

INSERT INTO key_type(type) VALUES('SIN');
INSERT INTO key_type(type) VALUES('BN');
INSERT INTO key_type(type) VALUES('ATS');


INSERT INTO keys(key,guid_id,program_id, type_id) VALUES ('111111111',1,1,1);
INSERT INTO keys(key,guid_id,program_id, type_id) VALUES ('321951741',1,2,2);
INSERT INTO keys(key,guid_id,program_id, type_id) VALUES ('T89987781',1,3,3);
INSERT INTO keys(key,guid_id,program_id, type_id) VALUES ('123698741',1,4,1);
INSERT INTO keys(key,guid_id,program_id, type_id) VALUES ('963852741AX201',1,5,2);


INSERT INTO linkage(type,key1,key2,confidence_level ) VALUES (1,1,2, 'Confirmed - 100%');
INSERT INTO linkage(type,key1,key2,confidence_level ) VALUES (1,3,4, 'Presumed - 75%');
INSERT INTO linkage(type,key1,key2,confidence_level ) VALUES (1,1,3, 'Presumed - 50%');
INSERT INTO linkage(type,key1,key2,confidence_level ) VALUES (1,4,5, 'Presumed - 25%');


INSERT INTO meta_data(key_id,meta_data,timestamp) VALUES
    (1,'NAME: Lionel Messi; PHONE: (647)999-9999; ADDRESS: 10 Barcelona Rd., ON CANADA;','N/A'),
    (2,'NAME: Leo Messi; PHONE: 6479999999; ADDRESS: 10-Barcelona Road., ON CANADA;','N/A'),
    (3,'NAME: Lionel Andres Messi; PHONE: (647)9999999; ADDRESS: 10 Barcelona Rd., ON CANADA;','N/A'),
    (4,'NAME: Lionel Andres Messi C.; PHONE: 647-999-9999; ADDRESS: 10 Barcelona Rd., ON CANADA;','N/A'),
    (5,'NAME: Lio; PHONE: (647)999-9999; ADDRESS: 10 Barcelona Rd., ON CANADA;','N/A');
    