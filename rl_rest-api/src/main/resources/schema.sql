CREATE TABLE IF NOT EXISTS program(
    id BIGINT IDENTITY(1,1),
    name VARCHAR(100) NOT NULL
);

CREATE TABLE IF NOT EXISTS linkage_type(
    id BIGINT IDENTITY(1,1),
    type VARCHAR(100) NOT NULL
);

CREATE TABLE IF NOT EXISTS key_type(
    id BIGINT IDENTITY(1,1),
    type VARCHAR(100) NOT NULL
);

CREATE TABLE IF NOT EXISTS guids(
    id BIGINT IDENTITY(1,1),
    guid VARCHAR(100) NOT NULL
);

CREATE TABLE IF NOT EXISTS keys(
    id BIGINT IDENTITY(1,1),
    key VARCHAR(100),
    type_id BIGINT,
    guid_id BIGINT,
    program_id BIGINT,
    FOREIGN Key (type_id) REFERENCES key_type(id),
    FOREIGN KEY (guid_id) REFERENCES guids(id),
    FOREIGN KEY (program_id) REFERENCES program(id)
);

CREATE TABLE IF NOT EXISTS linkage(
    id BIGINT IDENTITY(1,1),
    key1 BIGINT,
    key2 BIGINT,
    type BIGINT,
    confidence_level VARCHAR(100),
    FOREIGN KEY (key1) REFERENCES keys(id),
    FOREIGN KEY (key2) REFERENCES keys(id),
    FOREIGN KEY (type) REFERENCES linkage_type(id),
);

CREATE TABLE IF NOT EXISTS meta_data(
    id BIGINT IDENTITY(1,1),
    key_id BIGINT,
    meta_data VARCHAR,
    timestamp VARCHAR,
    FOREIGN KEY (key_id) REFERENCES keys(id)
);
